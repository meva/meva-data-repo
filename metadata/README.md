This directory contains various metadata regarding the MEVA data.

Contents include:

* [meva-clip-camera-and-time-table.txt](meva-clip-camera-and-time-table.txt), which provides camera and time synchronization for the released MEVA video clips. See [clip-table-readme.md](clip-table-readme.md) for more details.

* [meva-camera-daily-status.txt](meva-camera-daily-status.txt), which provides camera location (Interior or Exterior) for each camera for each day of the released data. Camera location is constant during each day, but may change day to day.

Subirectories include:

* [camera-models](camera-models): Camera models for the MEVA KF1 collection.

