#
# Converts DIVA outputs for the VIAME DIVE application.
#
# poc: roddy.collins@kitware.com
#

import sys
import os
import re
import optparse
import json
import yaml
import csv

#
# the parsing functions reduce the json, kpf, or CSV to a
# list of SpatialResult objects, which are then emitted as a VIAME csv
#

class SpatialResult:
    def __init__(self, id1, name, conf, start, end, box):
        self.id1 = id1
        self.name = name
        self.conf = conf
        self.start = start
        self.end = end
        self.box = box

def deduce_clip( fn ):
    #
    # Assume fn is either:
    #
    # 1) a KPF filename, i.e. /path/to/some/released.meva.clip.geom.yml
    #
    # or
    #
    # 2) a CSV filename, i.e. /path/to/some/released.meva.clip.csv
    #
    # ...return 'released.meva.clip'
    #
    p = os.path.basename(fn).split('.')
    if p[-1] == 'yml':
        if len(p) < 3:
            sys.stderr.write('Err: Cannot deduce clip stem from "%s"\n' % (fn ))
            sys.exit(1)
        return '.'.join(p[:-2])
    elif p[-1] == 'csv':
        return '.'.join(p[:-1])
    else:
        sys.stderr.write('Err: Cannot deduce clip stem from unknown extension in "%s"\n' % (fn))
        sys.exit(1)

def parse_nist( fn, clip, suffix, entries, maxID):
    #
    # fn: the json filename
    # clip: 'all' to take all clips, or the stem of the clip to extract
    # suffix: string to append to all activity names (may be empty)
    # entries: the current list of SpatialResults
    # maxID: highest activity ID in entries when called
    #

    fh = open( fn, 'r' )
    d = json.load( fh )
    fh.close()

    id_counter = maxID+1
    for a in d['activities']:

        # extract the filename, check against the clip
        loc = a['localization']
        if len(loc.keys()) != 1:
            sys.stderr.write('Warn: %s has more than one file localization?\n' % str(a))
        loc_fn = list(loc.keys())[0]
        if clip != 'all' and (re.search( clip, loc_fn ) is None):
            continue

        # it passed; add a SpatialResult for each object in this activitiy
        for obj in a['objects']:
            obj_loc = obj['localization']
            file_level_keys = list( obj_loc.keys() )
            if len(file_level_keys) != 1:
                sys.stderr.write('Err: logic error; object localization has %d keys?\n' %
                                 (len(file_level_keys)))
                sys.exit(1)

            frame_dict = obj_loc[ file_level_keys[0]]
            frames = sorted( [int(x) for x in list(frame_dict.keys())])

            for frame_index in range(0, len(frames)-1):
                k0 = frames[ frame_index ]
                k1 = frames[ frame_index+1 ]
                box = frame_dict[str(k0)]['boundingBox']
                box_str = '%0.2f,%0.2f,%0.2f,%0.2f' % (
                      float(box['x']),
                      float(box['y']),
                      float(box['x'])+float(box['w']),
                      float(box['y'])+float(box['h']))

                act_name = a['activity']+suffix
                if 'presenceConf' in a:
                    presenceConf = a['presenceConf']
                else:
                    presenceConf = frame_dict[str(k0)]['presenceConf']

                entries.append( SpatialResult( id_counter,
                                               act_name,
                                               presenceConf,
                                               k0,
                                               k1,
                                               box_str ))
            id_counter += 1

    return (id_counter, entries)

def wrangle_kpf_fn( fn ):
    #
    # fn: either a .geom.yml or .activity.yml filename
    # verify and return both the .geom.yml and .activity.yml filenames
    #
    p = fn.split('.')
    stem = '.'.join(p[:-2])
    geom_fn = stem+'.geom.yml'
    act_fn = stem+'.activities.yml'
    for f in [geom_fn, act_fn]:
        if not os.path.exists( f ):
            sys.stderr.write('Error: %s not found\n' % f)
            sys.exit(1)
    return (geom_fn, act_fn)

def load_geom( geom_fn ):
    #
    # return a dictionary of id1 -> (dict of frame -> box_str)
    #
    boxes = dict()

    sys.stderr.write('Info: loading %s...\n' % geom_fn)
    fh = open(geom_fn, 'r')
    g = yaml.load( fh, Loader=yaml.Loader )
    sys.stderr.write('Info: loaded\n')
    for r in g:
        if not 'geom' in r:
            continue
        rec = r['geom']
        frame = rec['ts0']
        id1 = rec['id1']
        if not id1 in boxes:
            boxes[id1] = dict()
        box_coords = rec['g0'].split()
        box_str = '%0.2f,%0.2f,%0.2f,%0.2f' % (
            float(box_coords[0]),
            float(box_coords[1]),
            float(box_coords[2]),
            float(box_coords[3]));
        boxes[id1][frame] = box_str

    return boxes


def parse_kpf( fn, kpfAct, suffix, entries, maxID ):
    #
    # fn: either a geom.yml or activities.yml file (the other must be in the same dir)
    # kpfAct: the kpf packet for the activity (act2, act3)
    # suffix: string to append to activity names (may be empty)
    # entries: the current list of SpatialResults
    # maxID: highest activity ID in entries when called
    #

    (geom_fn, act_fn) = wrangle_kpf_fn( fn )
    boxes = load_geom( geom_fn )

    fh = open( act_fn, 'r')
    a = yaml.load( fh, Loader=yaml.Loader )
    fh.close()

    id1_counter = maxID + 1

    for r in a:
        if not 'act' in r:
            continue
        rec = r['act']

        act_name = list( rec[kpfAct].keys())[0]+suffix
        # for each actor, create a SpatialResult for each frame which is
        # in the geometry dictionary

        for actor in rec['actors']:
            # get the table of frames->boxes for this actor
            id1 = actor['id1']
            if not id1 in boxes:
                sys.stderr.write('Warn: actor %s in activity %s, but no geom entry?\n' %
                                 ( id1, str(r) ))
                continue

            actor_boxes = boxes[id1]

            # get the starting, ending frames from the tsr0 packet
            tsr0 = [x for x in actor['timespan'] if 'tsr0' in x][0]
            frames = tsr0['tsr0']
            for frame in range( frames[0], frames[1]+1 ):
                if frame in actor_boxes:
                    # create a one-frame spatial record... not the most efficient
                    entries.append( SpatialResult( id1_counter,
                                                   act_name,
                                                   1.0,
                                                   frame,
                                                   frame,
                                                   actor_boxes[frame] ))
            id1_counter += 1

    return (id1_counter, entries)

def parse_csv( fn, entries, maxID ):
    #
    # fn: a pre-cached viame CSV
    # entries: the current list of SpatialResults
    # maxID: highest activity ID in entries when called
    #

    maxID += 1
    newMaxID = maxID
    with open( fn ) as f:
        reader = csv.reader( f )
        for row in reader:
            # skip comments
            if row[0][0] == '#':
                continue
            if len(row) != 11:
                sys.stderr.write('Err: %s line %d: found %d fields, expected 11\n' % (
                    fn, reader.line_num, len(row)))
                sys.exit(1)

            (id1, ts_str, ts0, x1, y1, x2, y2, box_conf, length, act_name, act_conf) = (
                row[0],
                row[1],
                int(row[2]),
                float(row[3]),
                float(row[4]),
                float(row[5]),
                float(row[6]),
                row[7],
                row[8],
                row[9],
                float(row[10]) )

            box_str = '%0.2f,%0.2f,%0.2f,%0.2f' % ( x1, y1, x2, y2)
            # use maxID as an offset
            thisID = int(id1)+maxID
            entries.append( SpatialResult( thisID,
                                           act_name,
                                           act_conf,
                                           ts0,
                                           ts0,
                                           box_str ))
            if thisID > newMaxID:
                newMaxID = thisID

    return (newMaxID+1, entries)

def process_file( fn, suffix, clip, entries, maxID, kpfAct ):
    #
    # Process the file fn, which may be either NIST json (possibly with multiple
    # clips) or KPF (which will contain only one clip.) Read the activities into
    # the entries list, adjusting the ID to avoid collisions and adding the suffix.
    #

    if len(fn)==0:
        return( maxID, entries)

    fn_suffix = fn.split('.')[-1]
    if fn_suffix == 'json':
        sys.stderr.write('Info: parsing "%s" as NIST json\n' % (fn))
        (m, e) = parse_nist( fn, clip, suffix, entries, maxID )

    elif fn_suffix == 'yml':
        sys.stderr.write('Info: parsing "%s" as KPF with activity domain %s\n' % (fn, kpfAct))
        (m, e) = parse_kpf( fn, kpfAct, suffix, entries, maxID )

    elif fn_suffix == 'csv':
        sys.stderr.write('Info: parsing "%s" as viame csv\n' % (fn))
        (m, e) = parse_csv( fn, entries, maxID )

    else:
        sys.stderr.write('Err: No parser for "%s" files\n' % (suffix))
        sys.exit(1)

    sys.stderr.write('Info: %d entries; max ID now %d\n' % (len(e), m))
    return (m, e)

if __name__ == '__main__':

    parser = optparse.OptionParser( usage='usage: %prog [options] > viame.csv' )
    parser.add_option( '-s', '--sys', default='', dest='sysFN', help='DIVA system output file' )
    parser.add_option( '-g', '--gt', default='', dest='gtFN', help='DIVA ground-truth file' )
    parser.add_option( '-j', '--join', default='', dest='joinFN', help='cached viame.csv to append')
    parser.add_option( '--sys-suffix', default='', dest='sysSuffix', help='Apply this suffix to system activity names' )
    parser.add_option( '--gt-suffix', default='', dest='gtSuffix', help='Apply this suffix to ground truth activity names' )
    parser.add_option( '--kpf-act', dest='kpfAct', default='act2', help='KPF activity tag (kpf2, kpf3, ... ')
    parser.add_option( '--clip', dest='clip', default='auto', help='(json input) Select only from this clip; auto to deduce from kpf name; all to take them all' )

    (options, args) = parser.parse_args()

    # sanity checks
    if (len(options.sysFN)==0) and (len(options.gtFN)==0) and (len(options.joinFN)==0):
        sys.stderr.write('Err: must set at least one of -s or -g or -j\n')
        sys.exit(1)

    # NIST json output may include output from multiple clips. We can automatically
    # deduce which clip to select from the KPF or CSV filename if given.
    if options.clip == 'auto':
        if len(options.gtFN) != 0:
            new_clip = deduce_clip( options.gtFN )
        elif len(options.joinFN) != 0:
            new_clip = deduce_clip( options.joinFN )
        else:
            sys.stderr.write('Err: cannot use "auto" for --clip without setting -g or -j\n')
            sys.exit(1)

        sys.stderr.write('Info: deduced clip "%s" for JSON extraction\n' % (new_clip))
        options.clip = new_clip

    entries = list()
    maxID = -1

    (maxID, entries) = process_file( options.sysFN, options.sysSuffix, options.clip, entries, maxID, options.kpfAct )
    (maxID, entries) = process_file( options.gtFN, options.gtSuffix, options.clip, entries, maxID, options.kpfAct )
    (maxID, entries) = process_file( options.joinFN, '', '', entries, maxID, '')

    # output

    for e in entries:
        for frame in range( e.start, e.end+1 ):
            sys.stdout.write('%d,stem,%d,%s,%f,-1,%s,%f\n' % (
                e.id1,
                frame,
                e.box,
                e.conf,
                e.name,
                e.conf ))
# all done!
